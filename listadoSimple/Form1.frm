VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   4665
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   8325
   LinkTopic       =   "Form1"
   ScaleHeight     =   4665
   ScaleWidth      =   8325
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox List1 
      Height          =   2400
      Left            =   1080
      TabIndex        =   0
      Top             =   720
      Width           =   4455
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    'Se declara el objeto para la conexion
    Dim conexion As New ADODB.Connection
    'se declara un objeto para que se pueda "recorrer" o ver los registros
    Dim rst As New ADODB.Recordset
    'Se asigna el tipo de conexion y la ubicacion fisica del archivo
    conexion.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=visualbasic.mdb;Persist Security Info=False"
    'La siguiente linea establece la conexion
    conexion.Open

    Set rst = conexion.Execute("SELECT * FROM personas")
    
    Do While Not rst.EOF
        List1.AddItem rst!nombre
        rst.MoveNext
    Loop
    
    rst.Close
    conexion.Close
End Sub
